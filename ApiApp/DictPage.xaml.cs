﻿using System;
using System.Collections.Generic;
using ApiApp.Models;
using Xamarin.Forms;

namespace ApiApp
{
    public partial class DictPage : ContentPage
    {
       

        public DictPage(string getWordSwag, Welcome wordData)
        {
            InitializeComponent();

            Word.Text = getWordSwag;

            definition.Text = wordData.Definition;

            //example.Text = wordData.Example;

            //type.Text = wordData.Type;
        }


    }
}
