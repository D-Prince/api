﻿using Xamarin.Forms;

namespace ApiApp
{
    public partial class ApiAppPage : ContentPage
    {
        public ApiAppPage()
        {
            InitializeComponent();
        }

        void goToConnect(object sender, System.EventArgs e)
        {
            Navigation.PushAsync(new ConnectionPage());
        }

        void ApiPage(object sender, System.EventArgs e)
        {
            Navigation.PushAsync(new ApiStuff());
        }
    }
}
