﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using ApiApp.Models;
using System.Linq;
using Xamarin.Forms;
using Newtonsoft.Json;

namespace ApiApp
{
    public partial class ApiStuff : ContentPage
    {
        public ApiStuff()
        {
            InitializeComponent();

        
        }



        async void ButtonClicked(object sender, System.EventArgs e)
        {
            HttpClient client = new HttpClient();

            var getWordSwag = word.Text.ToString();


            var uri = new Uri(string.Format($"https://owlbot.info/api/v2/dictionary/word" + getWordSwag));

            var request = new HttpRequestMessage();
            request.Method = HttpMethod.Get;
            request.RequestUri = uri;
            request.Headers.Add("Application", "application / json");

            HttpResponseMessage response = await client.SendAsync(request);

            Welcome wordData = null;
            if (response.IsSuccessStatusCode)
            {
                var content = await response.Content.ReadAsStringAsync();
                wordData = Welcome.FromJson(content).FirstOrDefault();
                await Navigation.PushAsync(new DictPage(getWordSwag, wordData));
            }
        }
    }
}
