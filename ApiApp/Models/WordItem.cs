﻿

namespace ApiApp.Models
{
    
        using System;
        using System.Collections.Generic;
    using System.Net;

        using System.Globalization;
        using Newtonsoft.Json;
        using Newtonsoft.Json.Converters;

        public partial class Welcome
        {
            [JsonProperty("type")]
            public string Type { get; set; }

            [JsonProperty("definition")]
            public string Definition { get; set; }

            [JsonProperty("example")]
            public string Example { get; set; }
        }

        public partial class Welcome
        {
        public static Welcome[] FromJson(string json) => JsonConvert.DeserializeObject<Welcome[]>(json, ApiApp.Models.Converter.Settings);
        }

        public static class Serialize
        {
        public static string ToJson(this Welcome[] self) => JsonConvert.SerializeObject(self, ApiApp.Models.Converter.Settings);
        }

        internal static class Converter
        {
            public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
            {
                MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
                DateParseHandling = DateParseHandling.None,
                Converters = {
                new IsoDateTimeConverter { DateTimeStyles = DateTimeStyles.AssumeUniversal }
            },
            };
        }

}

